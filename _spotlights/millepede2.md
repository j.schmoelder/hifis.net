---
layout: spotlight

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: Millepede II
date_added: 2022-11-02

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: millepede2/mp2-logo.png

# One or two sentences describing the software
excerpt: > 
    Millepede II has been developed to solve the linear least squares problem with a simultaneous fit of all global and local parameters,
    irrespectively of the number of local parameters.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image: default

# Title at the top, inside the title-content-container
title: Millepede II

# Add at least one keyword
keywords:
    - Alignment
    - Tracker
    - Large linear equation system

# The Helmholtz research field
hgf_research_field: Matter

# At least one responsible centre
hgf_centers:
    - Deutsches Elektronen-Synchrotron DESY

# List of other contributing organisations (optional)
contributing_organisations:
    - name: Universität Hamburg
      link_as: https://www.uni-hamburg.de/

# List of scientific communities
scientific_community:
    - High energy physics

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: claus.kleinwort@desy.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: gitlab
      link_as: https://gitlab.desy.de/claus.kleinwort/millepede-ii

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: LGPL-2.0-only

# Is the software pricey or free? (optional)
costs: Free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    -

# The application type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    -

# List of programming languages (optional)
programming_languages:
    - Fortran
    - C++

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi:

# Funding of the software (optional)
funding:
    - shortname: DESY

---

# Millepede II

In certain least squares fit problems with a very large number of parameters, the set of parameters can be divided into two classes: Global and local parameters.
Local parameters are only relevant in subsets of the data.
Contrary, detector alignment and calibration based on track fits are amongst the problems
where the interest is in optimal values of the global parameters,
the alignment parameters.

The method, called Millepede, to solve the linear least squares problem with a simultaneous fit of all global and local parameters, irrespectively of the number of local parameters, has been developed as experiment-independent programs by Prof. V. Blobel (Univ. Hamburg).

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/millepede2/mp2-eqn-sys.png" alt="The complete linear equation system with a (huge) bordered band matrix.">
<span>The complete linear equation system with a (huge) bordered band matrix.</span>
</div>

The current Millepede II version consists of two parts: "mille" and "pede".
The mille step has to be integrated in the experiment specific tracking software to produce dedicated binary files containing all the information needed to build the linear equation system described the (linearised) alignment problem.

The standalone "pede" executable is then used to build and solve that equation system.

It has been highly optimised by exploring multiple threading with OpenMP and dedicated linear algebra libraries (LAPACK).
