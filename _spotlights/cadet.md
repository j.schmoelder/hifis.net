---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: CADET

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2023-02-11

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: cadet/cadet_logo.png

# One or two sentences describing the software
excerpt: >
    CADET is a fast and accurate solver for a comprehensive model family. Typical applications include (but are by far not limited to) chromatography, filtration, crystallization, and fermentation. The models are solved with state-of-the-art mathematical algorithms and scientific computing techniques.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: CADTE

# Add at least one keyword
keywords:
    - modeling
    - simulation
    - chromatography
    - optimization
    - adsorption
    - downstream processing
    - compartment modelling

# The Helmholtz research field
hgf_research_field: Earth&Environment

# At least one responsible centre
# Please use the full and official name of your centre
hgf_centers:
    - Forschungszentrum Jülich

# List of other contributing organisations (optional)
contributing_organisations:
    - name: # Name
      link_as: # Link (optional)

# List of scientific communities
scientific_community:
    - Chromatography
    - Adsorption
    - CFD

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: cadet@fz-juelich.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: github
      link_as: https://github.com/modsim/cadet
    - type: webpage
      link_as: https://cadet.github.io
    - type: discourse
      link_as: https://forum.cadet-web.de

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: GPL-2.0-or-later

# Is the software pricey or free? (optional)
costs: free and open

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - simulation

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop
    - HPC

# List of programming languages (optional)
programming_languages:
    - C++
    - Python

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
# If you have one doi, please provide it as a variable:
doi: https://doi.org/10.1016/j.compchemeng.2018.02.025

# If you have multiple products, e.g. in a framework, and you want to reference each product with a DOI,
# then please provide a dictionary. Uncomment the following lines:
#     - name: # Product name for first DOI
#       doi: # DOI


# Funding of the software (optional)
funding:
    - shortname: FZ Jülich
    - shortname: Industry Sponsors
    - shortname: Inno4Vac
      funding_text: Innovations to accelerate vaccine development and manufacture 
      link_as: https://www.inno4vac.eu/
---
## CADET
    
CADET is developed at the Institute of Bio- and Geosciences 1 (IBG-1) of Forschungszentrum Jülich (FZJ).
The heart of the CADET software is a fast and accurate solver for a comprehensive model family.
Typical applications include (but are by far not limited to) chromatography, filtration, crystallization, and fermentation.
CADET can handle arbitrary sequences and networks of unit operations, including reactors, tanks, tubes, pumps, valves, detectors, etc.
The resulting models are solved with state-of-the-art mathematical algorithms and scientific computing techniques.
In addition to the solver, tools for parameter estimation and process optimization are provided.

**Features:**
- Fast and accurate solution of strongly coupled partial differential algebraic equations (PDAE)
- Computation of parameter sensitivities with algorithmic differentiation (AD)
- Shared memory parallelization using Intel TBB
- Python interface (recommended) and native MATLAB interface (deprecated)
- Support of HDF5 and XML data formats
- Flexible and extensible through modular design
- Works on Windows, Linux, and Mac OS X

**Abbildung:**
Aus standard-ppt.

<div class="spotlights-text-image">
<img alt="Chromatography Model" src="{{ site.directory.images | relative_url}}spotlights/cadet/chromatography_model.png">
<span>Chromatography Model.</span>
</div>

