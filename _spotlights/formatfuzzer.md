---
layout: spotlight

###############################################################################
# Template for Software spotlights
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: FormatFuzzer

# The date when the software was added to the spotlights YYYY-MM-DD
date_added: 2022-07-25

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: formatfuzzer/FormatFuzzer.png

# One or two sentences describing the software
excerpt: > 
    FormatFuzzer is a framework for high-efficiency, high-quality generation  
    and parsing of binary inputs. It takes a binary template that describes 
    the format of a binary input and generates an executable that produces 
    and parses the given binary format. From a binary template for GIF, for 
    instance, FormatFuzzer produces a GIF generator - also known as GIF 
    fuzzer.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: FormatFuzzer – Efficiently Generate High-Quality Binary Inputs

# Add at least one keyword
keywords:
    - Software Engineering
    - Software Testing
    - Fuzzing
    - Secure Software
    - Automated Test Generation

# The Helmholtz research field
hgf_research_field: Information

# At least one responsible centre
# Please use the full and official name of your centre
hgf_centers:
    - CISPA Helmholtz Center for Information Security

# List of other contributing organisations (optional)
contributing_organisations:
#    - name: 
#      link_as:

# List of scientific communities
scientific_community:
    - Software Engineering
    - Software Testing

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: zeller@cispa.de

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: webpage
      link_as: https://uds-se.github.io/FormatFuzzer/
    - type: github
      link_as: https://github.com/uds-se/FormatFuzzer

# The software license, please use an SPDX Identifier (https://spdx.org/licenses/) if possible (optional)
license: GPL-3.0-only

# Is the software pricey or free? (optional)
costs: free

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:

# The applicaiton type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    - Desktop

# List of programming languages (optional)
programming_languages:
    - Python
    - C++

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi:

# Funding of the software (optional)
# funding:
#    - shortname: # Abbreviation
#    funding_text: # Short text or sentence, if required by your funding guidelines (optional)
#      link_as: # Link (optional)
---

# FormatFuzzer – Efficiently Generate High-Quality Binary Inputs

Software has bugs, and catching bugs can involve lots of effort. Fuzzing addresses this problem by automating software testing, specifically by generating tests automatically. Effective fuzzing of programs that process structured binary inputs, such as multimedia files, is a challenging task, since those programs expect a very specific input format. Existing fuzzers, however, are mostly format-agnostic, which makes them versatile, but also ineffective when a specific format is required.

FormatFuzzer is a framework for high-efficiency, high-quality generation and parsing of binary inputs. It takes a binary template that describes the format of a binary input and generates an executable that produces and parses the given binary format. From a binary template for GIF, for instance, FormatFuzzer produces a GIF generator - also known as GIF fuzzer.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/formatfuzzer/gif-fuzzer.gif" alt="Invoking FormatFuzzer">
<span>FormatFuzzer is a framework for high-efficiency, high-quality generation and parsing of binary inputs.</span>
</div>

Generators produced by FormatFuzzer are highly efficient, producing thousands of valid test inputs per second - in sharp contrast to mutation-based fuzzers, where the large majority of inputs is invalid. By default, FormatFuzzer operates in black-box settings, but can also integrate with AFL++ to produce valid inputs that also aim for maximum coverage.
