**We _still_ want your use case:**
Probably you are already using some of our HIFIS services in your work.
Want to tell other communities about it?
Great, we highly appreciate it, because as HIFIS, we would like to help you cross-connect with other communities!

**Get in contact with us** via <support@hifis.net>, stating:

1. a brief description of your project
2. information about which HIFIS services you use and to which purpose
3. contact information (name and mail) to be published alongside the text.

Happy HIFIS using!
