**Looking back at the Incubator Summer Academy:**
The five Incubator platforms Helmholtz AI, Helmholtz Imaging, HIDA, HMC and HIFIS
have teamed up to create an exciting program at the Incubator Summer Academy.
[Read more in this post]({% post_url 2022/2022-10-13-incubator-summer-academy %})
where we take a look back at an intense two-week program.
