---
date: 2023-09-01
title: Tasks in September 2023
service: overall
---

## Elaborate on long-term plans
During [HIFIS evaluation]({% post_url 2022/2022-12-12-evaluation-update %}), we were given advise to elaborate on some of the extended future plans of HIFIS, to be put in place from 2025 onwards.
These further details are planned to be worked out until fall 2023.
