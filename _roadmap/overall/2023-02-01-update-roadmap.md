---
date: 2023-02-01
title: Tasks in February 2023
service: overall
---

## Based on evaluation: Define further developments
Following the overall very positive [HIFIS evaluation results]({% post_url 2022/2022-12-12-evaluation-update %}) received end of 2022 and
in line with our ongoing [annual reporting]({% link reports.md %}),
we will define the short- and mid-term steps to focus on during 2023 and parts of 2024.
