---
date: 2022-10-31
title: Tasks in October 2022
service: cloud
---

## The draft of the GDPR agreement is submitted to the Helmholtz Assembly of Members 
In close cooperation with the data protection officers of the Helmholtz Association, we have developed a joint controller agreement that covers the Helmholtz Cloud core components that operate the cloud.


