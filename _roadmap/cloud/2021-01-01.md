---
date: 2021-01-01
title: Tasks in Jan 2021
service: cloud
---

## Top 10 of future Helmholtz Cloud Services 
We are happy to announce that the group of the 10 most mature services has reached a maturity level of more than 80% - more than sufficient for them to be integrated into the Cloud Portal in the coming weeks. This top group includes services like **GitLab** (HZDR, FZJ), **Nextcloud** (HZB, KIT), **Mattermost** (HZDR), **Rocket.Chat** (FZJ) but also **OpenStack** (FZJ, KIT), **JupyterHub** (FZJ), **Zammad** (HZDR) and **B2Share** (FZJ).

Find more information of Initial Service Portfolio [here](https://hifis.net/news/2020/10/13/initial-service-portfolio.html).
