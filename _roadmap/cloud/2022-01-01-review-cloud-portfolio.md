---
date: 2022-01-01
title: Tasks in Jan 2022
service: cloud
---

## Initial Review of Cloud Service Portfolio
The primary goal is to check whether the services in and the processes around the Service Portfolio continue to fulfill defined requirements. Consequently, the review may lead to changes in the Service Portfolio itself as well as the Process Framework. The results will enter the annual HIFIS report and further Service Portfolio Maintenance.
