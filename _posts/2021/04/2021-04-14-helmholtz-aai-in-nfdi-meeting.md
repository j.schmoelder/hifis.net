---
title: "Helmholtz AAI Update in the context of AARC, EOSC, NFDI"
title_image: default
data: 2021-04-14
authors:
  - "Marcus Hardt"
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    Helmholtz AAI is briefly pitched in the 74. DFN Betriebstagung.
---

# Update on Helmholtz AAI

[**See here**](http://marcus.hardt-it.de/2104-nfdi-aai) for an updated presentation on the role of Helmholtz AAI in the context of AARC, EOSC, and NFDI.

#### Goals
With this, you'll be able to

* Understand what AAI can do
    * Including the VO Concept
* Understand what AAI can not do
* Understand the general architecture
    * EOSC AAI
    * Helmholtz AAI
    * Your AAI

#### Bottomline
* **Do not build your own AAI!**
    * you are likely to repeat stone, bronze, and iron-age 
* Some communities are already organised
    * i.e. they run their own community AAI (which is fine) 
* Users always live in the context of their community AAI
    * Makes it difficult to cooperate across community borders
* But: Users can be members of multiple communities 
