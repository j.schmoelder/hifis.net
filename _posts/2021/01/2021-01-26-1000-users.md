---
title: "Welcoming more than 1000 users from almost all Helmholtz"
title_image: default
data: 2021-01-26
authors:
  - jandt
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    Helmholtz AAI has now more than 1000 registered users. Welcome!
---

# More than 1000 users

We welcome more than 1000 users in Helmholtz AAI. These users originate from almost all Helmholtz centres.

Thank you for using Helmholtz AAI and our [**pilot services**](https://hifis.net/doc/service-integration/pilot-services/pilot-services/)!

### AAI high-level usage statistics

The statistics of Helmholtz AAI usage can be found in this Gitlab Project:

* <https://codebase.helmholtz.cloud/hifis/communication/service-usage-plots>

See also the following plot on
* Number of connected Helmholtz centres
* AAI-connected services
* Registered end users
* Registered Virtual Organisations (VO)

![Plot of Helmholtz AAI](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/raw/9cc4900aa625ad26f298e21f9d40eaa345180f39/plots/plot.png)
