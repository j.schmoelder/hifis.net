---
title: "16 of 18 Helmholtz centres plus Head Office connected"
title_image: default
data: 2021-02-09
authors:
  - jandt
layout: blogpost
categories:
  - News
tags:
  - Helmholtz AAI
excerpt: >
    16 of 18 Helmholtz centres plus Head Office meanwhile have an IdP in Helmholtz AAI and thus joined the big EduGAIN community.
---

# Connected centres

After a burst in late 2020, there are meanwhile 16 out of 18 Helmholtz centres, plus the Helmholtz Head Office, connected to the Helmholtz AAI, thus allowing most of Helmholtz to join the EduGAIN community.

This implies that all Identity Providers (IdP) provide sufficient attributes about their users and there are actually active users already registered in the AAI.

**We look forward to many new users from the newly connected centers to use our [pilot services](https://hifis.net/doc/service-integration/pilot-services/pilot-services/) and the upcoming [Helmholtz Cloud services](https://cloud.helmholtz.de/#/services)!**
