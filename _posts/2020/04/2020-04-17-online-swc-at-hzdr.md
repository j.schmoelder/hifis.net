---
title: "Our First Online SWC Workshop"
date: 2020-04-17
authors:
  - erxleben
  - huste
  - hueser
layout: blogpost
title_image: default
categories:
  - Report
tags:
  - workshop
excerpt:
    "
    It was supposed to be our first Software Carpentries workshop at the HZDR.
    We were in full swing organizing a live event until it became clear that we
    would have to move online. <em>Challenge accepted!</em>
    "
---

### Contents
{: .no_toc}

1. TOC
{:toc}



# Planning Phase

Our first own [Software Carpentry](https://software-carpentry.org/) workshop was
supposed to be a live event.
We intended to take it easy, learn a few lessons and then build upon these.
With these goals in mind we set out to plan an two-day workshop for the
31st of March and the 1st of April 2020.
In the beginning of March it became clear that the effects of the Covid-19
pandemic would reach us long before this date.
It was unanimously decided to switch the workshop to an online event instead of
cancelling it — even though this would mean a lot of organizational work with an
increasingly tight deadline.

## Our Original Approach

As it was expected to be a first experience for us as instructors, organizers
and helpers, we advertised 25 workshop seats on the PhD mailing list of
our institute.
To our complete surprise the event became booked out within the day.
As a reaction we created a second "event" in our system to act as a waiting
list and to be worked off in follow-up workshops.
The side effect was that we got a first glimpse of the huge demand for training
opportunities by our scientific staff.

Our initial plan was to split the first workshop day equally between the
_Shell_ (the [_Bash_](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) to be
precise) and _Git_ lessons and use the complete
second day for _Python_.

We did however estimate that we might have to cut later episodes from these
lessons, depending on the learners' speed.

## Switch to Online

When we decided to switch to an online event it was clear that there are
additional unknowns to be expected.
For this reason we decided to reduce the number of participants for the first
iteration of the workshop to only 9 persons.
As we had no prior experience with online teaching it seemed better to start
with a conservative amount of participants and increase the number in future
workshops if everything went well.

Therefore we set up a separate event in our system and transferred the planned
amount of participants over, based on the first-come-first-serve principle.
The remaining participants will be enrolled with priority into a follow-up
workshop in April 2020.

Additional time had to be spent on selecting and organizing a suitable
video-conferencing system.

{:.treat-as-figure}
![Image: Example Setup]({{ site.directory.images | relative_url }}/posts/2020-04-17-online-swc-at-hzdr/view_from_home.jpg "Example Setup")
An example setup. The second monitor proved very useful.

# Role Call

While in the live-setting one _instructor_ is supported by one or multiple
_helpers_, the online environment also demands one of the helpers to
additionally take on the role of the _host_.
The roles can change from lesson to lesson since especially the instructor has a
high cognitive load and may be subject to a strained voice after prolonged
periods of teaching.

## Instructor
The primary task of instructors is to lead the workshop.
They present the content of the teaching material, determine the pace of the
workshop and coordinate with the hosts and helpers.
Thus, it is especially important for them to familiarize with the workshop
materials and manage the time required for teaching episodes, exercises and
breaks.

## Host
Running a workshop also requires to complete a lot of organizational side-tasks
during the event.
The nature of these tasks changes significantly when switching from a live event
to an online workshop.
Notably, video-conferencing tools tend to determine one person as _host_ who has
the full management rights for the event.
To reduce the instructors' workload, a seperate person fulfils the role of the
_host_ who can take over these tasks during the session:

* Prepare and open breakout rooms
* Monitor the chat (together with the helpers)
* Keep an eye on the time
* Observe the participants reactions
* Organize quick polls for feedback or exercises
* Manage shared documents and insert exercise questions on demand

In general the host is less focused on the participants but on the instructor
and helpers, taking note of the lesson progress and anticipating required
organizational actions.

## Helper
Helpers are the backbone of a successful workshop.
They monitor the participants and proactively interact with participants that
have questions, may fall behind or have technical issues.
Questions may either be answered by helpers directly or be forwarded to the
instructor in an opportune moment if they are of more general concern.

## The Workshop Team
We split our workshop into the three parts _Shell_, _Git_ and _Python_ between
our three instructors.
The two instructors who were not actively teaching, assumed the roles of host
and helper respectively.
We further expanded our team by two helpers which allowed us to
respond to questions without delay.

# The Tools

The choice of tools significantly affects the organizational effort and workshop
quality perceived by the participants.
In the following our selected tools will be shortly introduced.

## Indico

We employed [our self-hosted _Indico_ instance](https://hifis-events.hzdr.de)
as the event planning and registration tool.
It proved to be a good choice to facilitate the registration procedure and
allows to message selected (or all) event participants directly which turned out
to be very useful when switching the workshops to the online version.

One drawback was the limited capability to transfer registrations from one event
to another, which had to be done manually, since the provided _export_ and
_import_ features did not support a common data layout.

> [Official Indico Website](https://getindico.io/)

## GitLab

It appeared to be a good idea to extend the _Git_-lesson and also give a quick
look at _GitLab_ as an example of a well-known web-based collaborative software
life-cycle tool.
Thereby, the participants were able to apply their acquired _Git_ knowledge to
the User-Interfaces of _GitLab_.
As most of our participants were members of the HZDR and we also had the
sufficient administrative rights to allow access for all other participants, we
chose to use the [institute-local _GitLab_ instance](https://codebase.helmholtz.cloud)
for this purpose.
In future workshops with participants from other institutions we might switch to
[_gitlab.com_](https://www.gitlab.com) for this exercise.

It is worth mentioning that people who signed in via a third-party provider need
to use an access-token when cloning via _https_.
This can also be the case on _gitlab.com_ and forces the organizers to plan some
time to get this set up for all affected participants.

## HackMD

Amongst the many collaborative online editors that were availabe we chose
_HackMD_ for its plentiful characteristics:

* Ease-of-use
* Markdown formatting capabilities
* Code / syntax highlighting
* Side-by-side editing and preview

Even though our participants had no previous experience with markdown documents,
they quickly adopted its basics.
Some exercises required the solutions to be put into code blocks or tables which
were either copied and pasted from prepared examples or formatted by the
helpers.

> [Official HackMD Website](https://hackmd.io)

## Zoom

The choice for a video-conferencing tool was probably the most important
decision during the switching to online.
We were already familiar with Zoom from the Carpentry instructor lessons and had
the good fortune to be offered a share in a Zoom-Account by another member of
the Carpentries for the purpose of holding workshops until we could organize our
own account.
There was not enough time to get a _BigBlueButton_ or _Jitsi_-instance
installed and evaluated properly.

During the workshop we could make good use of the offered features and
experienced good video and audio quality.
We prefixed the screen names of helpers and instructors with their respective
role to make them easier to distinguish by name alone.

In the light of rising security and data protection concerns regarding Zoom we
continue to monitor the situation and keep exploring alternatives with the aim
to offer the best possible workshop experience to our participants in a
privacy-friendly way.

## IPython

For teaching the basics of _Python_ we went with _IPython_.
It offers syntax highlighting to aid the learner.
Since it is an interpreter, the participants get instant feedback if the entered
line is valid python.
The command-line based approach significantly reduces the amount of objects on
the screen and aids to focus the learners attention on the code itself.

The tool comes with the standard installation of the _Anaconda_ packages as
recommended in the setup instructions by the Carpentries.

# How it Went

It became clear early on that an online workshop progresses notably slower
in comparison to a live version.
Due to the lack of non-verbal communication, it is more often required to assess
the learners progress and also, in the case of helpers, to interact with each
learner individually.
Sometimes communication can also be impeded by low quality audio transfers
making it necessary to repeat parts of sentences or write things down in a chat
or shared document.
We enjoyed a mostly stable connection with good quality and the participants
were very cooperative and disciplined, muting themselves if they did not wish to
speak.

To encourage users to use the shared document and signal that they are allowed
and expected to modify it, we included a warm-up exercise which asked the
participants to note down their attendance together with a short statement about
themselves.
"What is your favourite cake?" turned out to be a suitable icebreaker question.

The first quarter of the second day was used for a quick recap of the _Git_ and
_Shell_ lessons, followed by the _Git_ collaboration exercise with the aid of a
_GitLab_ repository.

The _Python_ part did not progress as fast as intended and led to the lessons
being compressed towards the end.

## Issues Encountered

### Operating System Differences
{: .no_toc}

The participants were, with one exception, using Windows.
While this required some additional research effort for the helpers on some
occasions it kept the overall effort low, since participants could also help
each other out if necessary.

Particular issues, for example accomodating for the different line ending
encodings, that would arise were also covered by the Carpentries' lecture
materials and could thus quickly be solved.

### Handling More Challenging Problems
{: .no_toc}

Two more complex issues could be solved by the host assigning the participant in
question to a breakout room together with a helper.
This way the lessons could progress for the unaffected participants and the
breakout room was joined back once the issues were resolved.

From the experiences made we would reserve such procedures only for the most
dire of problems since the involved learners lose the connection to the workshop
and may have trouble getting back on track.

It is preferrable to try helping in the shared online environment first, to
allow the other participants to either learn something new for themselves or
contribute on their own.

## Feedback, Reactions and Lessons Learned

The post-workshop survey determined that the participants viewed the event in a
positive light.
Compared to the pre-workshop survey, most participants felt enabled to make
first steps towards improving their workflows.

A general consensus between participants and organizers alike was the need to
plan more time for the workshop content as well as the demand for follow-up
workshops covering more advanced topics.

As organizers we consider this first event a success and a good foundation upon
which to build our future events.

## Acknowledgements

{% assign instructors="Steinbach, Peter / huste / erxleben" | split: " / "%}
{% assign helpers="Lokamani / hueser" | split: " / "%}

We want to thank our **instructors**
{% include team_card_mini_bundle.html persons=instructors %}

and **helpers**
{% include team_card_mini_bundle.html persons=helpers %}

for organizing and holding the workshop, as well as our **participants** for
being awesome learners.

_Until next time!_
