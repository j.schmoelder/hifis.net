---
title: "Guidelines for Video Conferencing"
title_image: chris-montgomery-smgTvepind4-unsplash.jpg
date: 2020-05-15
authors:
  - Hammitzsch, Martin (GFZ) et al.
layout: blogpost
categories:
  - Guidelines
excerpt:
redirect_from:
  - services/video_conferencing/index_ger.html
  - services/video_conferencing/index.html
  - guidelines/video_conferencing/index_ger.html
  - guidelines/video_conferencing/index.html
excerpt:
  You are the one in charge of making this call a success by means of that
  everyone is happy with a smooth performance of the call itself and is
  satisfied with the outcome, preferably actionable  results having a practical
  value.
  Using a video conferencing tool shall not hamper an effective meeting and
  should be used to make the virtual get-together as efficient as possible when
  participants are not at the same place.
lang: en
lang_ref: 2020-05-15-guidelines-for-video-conferencing
---

## Scope
We’re talking about _DFNconf_, _Skype_, _Google Hangouts_, _Microsoft Teams_,
_Cisco WebEx_, _Adobe Connect_, _Zoom_, _Jitsi Meet_, _BlueJeans_,
_GoToMeeting_, _StarLeaf_, _Lifesize_, _Fuze_ and other services that you may
have heard of supporting online meetings.

## Task
You're either responsible for a team, a project, or any other measure - or you
support someone who is.
Regular and extraordinary meetings with the persons involved are part of your
managerial routine to lead a group of individuals distributed across space and
time.

## Situation
You are going to run a conference call with remote team members to get or keep
things going - either by phone, or more likely, with a digital tool for video
conferencing.
Some participants may be in a hurry, may have other burning issues to take
care of, or may have experience with unsatisfying virtual meetings.
However, everyone has to coordinate with each other.

## Challenge
You are the one in charge of making this call a success by means of that
everyone is happy with a smooth performance of the call itself and is
satisfied with the outcome, preferably actionable  results having a practical
value.
Using a video conferencing tool shall not hamper an effective meeting and
should be used to make the virtual get-together as efficient as possible when
participants are not at the same place.

## Approach
When you set-up an agenda, refer to a netiquette and best practices that covers
a set of rules to make you and your team/group/colleagues feel comfortable and
enable a productive environment in the meeting. 

## Options
Below are a few suggestions that might make it into the set of your rules to
make you feel comfortable in a virtual meeting, and the others too.

### Equipment
- **Use a headset or at least headphones or external speakers made for conferencing**  
  Using speakers close to your ears or speakers that are made for conferencing
  avoid feedback of spoken words.

- **Use an external microphone or an external camera paired with a room microphone**  
  Internal microphones and internal cameras, especially of older computers,
  might be no fun for the other participants due to a limited quality of
  recorded sound and video signals.

- **Use a modern computer or mobile device**  
  Conferences tools, especially those supporting video, require hardware support
  that often is underestimated.
  Computers and mobile devices with modern technology are capable of handling
  modern video conferences.
  Older devices may lead to problems sooner or later.

### Connection
- **Use a high-speed Internet connection**  
  Preferably use a wired connection, stay close to a wifi device, or find a
  fixed place with a reliable mobile network.
  Avoid low-bandwidth connections shared with others streaming Netflix,
  downloading files or video chatting at the same time.

- **Close your VPN connection**  
  VPN connections route your communication through an additional transmission
  node becoming a potential bottleneck when everyone is doing the same.
  Disconnecting from VPN and similar connections reduces the risk of a
  slowed down internet connection.

- **Shut down any other programs connected to the network**  
  Normally many other tools run in the background that might interfere with
  the bandwidth necessary to participate in a high-quality video conference.
  So whenever you experience problems with the connection consider this option
  too.

- **Stop other devices streaming music or movies**  
  Tools on other devices may hamper your bandwidth, too,
  when in use at the same time.
  So close apps and programs for the time of the video conference even if
  your kids don’t like it.

### Conferencing tool
- **Use an up-to-date browser or application**  
  Make sure to use a browser supported by the provider of the conferencing
  service and double-check that updates are not too far behind.
  Installing the application offered by the service provider may help,
  too, to stay up-to-date. 
  Third party tools are an option but need a closer look since some of them
  miss the required maintenance.

- **Make yourself comfortable with the conferencing software**  
  Whether used in a browser or as an application, the conference tool of choice
  might have a huge range of functionality, a very own behaviour, and
  many settings. This needs to be understood.
  So make yourself comfortable in advance to understand the behaviour and
  turn-on/off functionality on-demand.

- **Master relevant settings of your operating system**  
  Conference tools offer lots of functionality and configurations.
  However, some of the important ones are anchored in operating systems such
  as Microsoft Windows, Mac OS X or a Linux distribution. Specifically,
  enabling and calibrating your microphone and speakers with the tools of your
  operating system should become routine shortly before a virtual meeting.
  A quick double-check testing the input level and volume might help to spot
  problems, too, e.g. caused by blocking bluetooth connections.

### Location
- **Find a great location and working place**  
  Use, and reserve if necessary, a quiet location instead of a busy café,
  the public transit, or the canteen.
  It helps you to stay focused, to keep unexpected disturbances out of the
  conference call, and to comfortably make use of working devices so that
  following media presented and working at documents either alone or
  collaboratively is fun.

- **Prepare your location**  
  Close the doors and preferably windows too keep traffic and construction
  noise, squeaky public transport and police sirens out of the conversation.
  Keep dogs and other pets out of your room to avoid unexpected disturbing
  sounds.

- **Set-up your camera**  
  Place the camera close to the screen or at least ensure that you look at the
  camera, not the screen, when talking.
  Provide space between your body and the background and reduce back-lightning
  by turning your computer and the conference equipment so you’re side-lit
  or front-lit.

### Pre-arrangements
- **Send out an agenda in advance**  
  Send a crisp agenda with topics, times and timeframes plus responsibilities
  and expected results and if required refer to related documents and
  other sources to allow preparation for the participants in advance and
  to have everything at hand in the video conference.

- **Send dial-in information**  
  Send dial-in details preferably together with the agenda.
  Sometimes it is needed to add alternatives to switch immediately in case of
  anticipated connection errors.
  Have in mind that this information is used often by participants when
  creating calendar entries.

- **Send a reminder**  
  Send a reminder with dial-in details again in the morning or shortly before
  the conference call so that participants don't waste time searching in
  e-mails from days ago.

### The first minutes
- **Open early**  
  Open the conference room five to ten minutes before the virtual meeting
  actually starts, if you are the host.

- **Enter early**  
  Enter the conference room a few minutes before the virtual meeting actually
  starts, if you are a participant, to test that your equipment works as
  expected - it's okay then to leave the scene, grab a coffee and show up in
  time again when the meeting finally starts.

- **Say Hi**  
  Unmute your microphone, switch-on video and say hello, your name and
  affiliation when entering the conference room to test audio and video quality
  and let the host and others know that you are here.

- **Mute your microphone**  
  Mute your microphone after the welcome procedure and whenever you do not speak.
  It reduces disturbances either coming from a noise background environment or
  coming from your keyboard while typing.

- **Turn off your camera**  
  Keep your camera turned on as long as you and the others feel comfortable.
  However, turning off the camera improves the audio quality considerably.
  Removing the video signal saves bandwidth which then is available for the
  audio signal.
  Depending on the rules, turning on the camera might be understood also as
  holding up your hand and, therefore, is a sign that you have a comment or
  want to say something at next.

- **Look out for an additional text channel**  
  Open the chat window of the conference room, have a look at it, and use it
  yourself to share important information, e.g. when problems with the
  communication occur or when locations to ressources have to be shared.

- **Double-check information about you**  
  Check if your name is entered correctly in the respective field so that the
  host and others know who you are, have a help to address you by name,
  and identify your contributions either when talking or typing in attached.

- **Stop playing with effects**  
  Conference tools offer options for visual and other effects that,
  unfortunately, consume computational resources that might be missing
  for the call itself.
  Blurring the background in the video is a useful option but already has been
  identified as the root of lags in video and audio signals.
  So if not necessary do not use effects in a professional call.

### Chairing
- **Introduce the rules**  
  After saying Hi when starting the conference, the host shall iterate quickly
  the selected conventions to follow, how to implement them in the conferencing
  tool, and ask if there are any questions.
  Participants take part in other video conferences, too, with slightly
  different rules and different tools.
  So a quick recap helps all to run a smooth virtual meeting.

- **Check participation**  
  The host should quickly go through the participants list in the conference
  tool by saying names and institutions so that everyone knows who is
  participating.
  Also name the persons and institutions missing to make everyone aware that
  particular discussions and decisions may miss relevant stakeholders.
  It also helps to keep these persons or groups up-to-date afterwards.
  Hosts should also ask the group if they missed naming someone either
  participating or being absent.

- **Implement the rules**  
  Everyone is invited to participate actively but the host is asked to chair
  the virtual meeting and thus provides orientation similar to a panel
  discussion or an evening talk show.
  The host should name the one person who is up to speak next.
  Participants follow what is said and use the functionality of the conference
  tool to signal that they have a comment or want to add something important,
  e.g. either by turning on their camera or by dropping a line in the chat.
  The host then picks up these “hand signs”, ensures a clear order who is
  next, and thus steers an organized discussion in the context of the agenda.

### Participating
- **Focus on the conference**  
  Following topics and discussions in a video conference is not that easy,
  especially when held in a different language than your mother tongue,
  when disturbed by a bad audio quality, and run for a longer time.
  Additionally, humans are fairly bad at multitasking.
  So give your full and undivided attention.

- **Speak with normal tone**
  Speak as you normally do with someone sitting across.
  Modern computers have decent microphones.
  They’re usually pretty good at picking up your voice,
  which means you don’t have to shout at your camera for other people to hear you.
  Just speak normally, and as long as there isn’t too much background noise,
  people will hear you.

- **Toggle your microphone when needed**  
  Unmute your microphone when you want to say something and mute it again once
  you are finished, you may do the same with the camera,
  it’s okay to toggle both all the time.

- **Turn off the camera**  
  When experiencing audio problems then removing the video signal saves
  bandwidth that improves the overall audio quality.
  So turning off your camera and additionally turning off the received video
  signal of others improves the audio quality considerably.

_These recommendations have been created in collaboration with
Tom Kwasnitschka (GEOMAR), Patrick Preuster (FZJ), Benjamin Rost (DZNE),
Carsten Schirnick (GEOMAR), Mario Strefler (KIT) and Tobias Vogt (HZDR)._

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>
