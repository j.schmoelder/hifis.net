---
title: "Join the Helmholtz GPU Hackathon!"
title_image: GPU_Hackathon_Banner_NoText_1920x300px.png
data: 2023-01-20
authors:
 - Fredo Erxleben
layout: blogpost
categories:
 - News
tags:
 - Event
 - HPC
excerpt: >
   Join the 2023 _Helmholtz GPU Hackathon_ and give your research software project a performance boost with the help of experienced mentors.
---

{:.alert .alert-success}
For application, more information & questions click [here](https://www.openhackathons.org/s/siteevent/a0C5e000007ZQ6lEAG/se000171).
Deadline for the team application is **March 14, 2023**!

## What is a GPU Hackathon?

GPU Hackathons provide exciting opportunities for scientists to accelerate their AI or HPC research under the guidance of expert mentors from national laboratories, universities and industry leaders in a collaborative environment.

Representing distinguished scholars and preeminent institutions around the world, these teams of mentors and attendees work together to realize performance gains and speedups using a variety of programming models, libraries, and tools.

The goal of the GPU Hackathon is for computational scientists to port, accelerate, and optimize their scientific applications to modern computer architectures, including CPUs, GPUs and other computing technologies. Participating teams should leave the event either with their applications accelerated and/or optimized on the latest supercomputing hardware or a clear roadmap of the next steps needed to leverage these resources.

## Who, When, Where?

!["Helmholtz GPU Hackathon, May 9 to 11, by FZJ, HIDA, HZDR, Nvidia, OpenAAC"]({% link assets/img/posts/2023-01-20-helmholtz-gpu-hackathon/GPU_Hackathon_Banner_Datum_1920x300px.png %})


Forschungszentrum Jülich (FZJ), Helmholtz Zentrum Dresden-Rossendorf (HZDR), and the Helmholtz Information & Data Science Academy (HIDA) together with NVIDIA and OpenACC will co-organize the Helmholtz GPU Hackathon 2023.

The event will be hosted by FZJ in Jülich (close to Cologne) **May 9 through 11, 2023** with a virtual kick-off on May 2, 2023.

The Helmholtz GPU Hackathon will be hosted **primarily onsite** at the Jülich Supercomputing Centre (JSC) in the Central European Summer Time (CEST) zone. JSC will be providing working spaces to host teams in person to give the attendees the most immersive Hackathon experience.

## More Info and Registration

You can find out more and register at the [website of the event](https://www.helmholtz-hida.de/en/events/helmholtz-gpu-hackathon-2023/).

{:.summary}
**Deadline for the team application is March 14, 2023!**

## Questions? Comments? Suggestions?
If you have any feedback to tell us, don't hesitate to [contact us](mailto:support@hifis.net).
