---
title: "deRSE23 Conference: Call for Contributions"
title_image: Logo_deRSE23.png
date: 2022-11-10
authors:
  - "Marco De Lucia"
layout: blogpost
categories:
  - News
tags:
  - Announcement
  - Conference
  - Event
  - RSE
excerpt: >
    deRSE23, the Third Conference for Research Software Engineering in Germany 
    will be held at the Heinz Nixdorf Forum in Paderborn on February 20-21, 2023. 
    The organization is accepting contributions for talks, demos, workshops. 

---

<div class="floating-boxes">

<div class="image-box align-right">
  <a href="https://de-rse23.sciencesconf.org/">
    <img
      class="right medium nonuniform"
      alt="deRSE23 logo"
      src="{% link assets/img/posts/2022-11-11-deRSE23/Logo_deRSE23.png %}"
      style="float:right;width:20%;min-width:320px;padding:5px 5px 20px 20px;"
    />
  </a>
</div>
   
<div class="text-box" markdown="1">

[deRSE23](https://de-rse23.sciencesconf.org/), the third conference in
Germany addressing research software and the people behind it within
the German research landscape will be held at the [Heinz Nixdorf
Forum](https://www.hnf.de/start.html) in Paderborn on February 20-21,
2023.

We welcome submissions (in English or German) from any and all people 
who have an interesting take on research software development. We are 
not just looking for seasoned presenters or people who are already well 
known. Thus, speakers from under-represented backgrounds and at early 
career stages are highly encouraged to submit a proposal. The aim is to
reflect the diverse community of research software engineers by seeking 
input from all levels of experience and across a variety of domains, 
geographic locations, genders, and ethnicities.

- **November 21, 2022 - Deadline for abstract submissions**
- December 12, 2022 - Open registration for conference
- December 22, 2022 - Notification of acceptance and program
- February 20-21, 2023 - deRSE23 conference

More informations - also in German - on the [submission page](https://de-rse23.sciencesconf.org/resource/page/id/6).

</div>
</div>
<!------------------------>

<div class="clear"></div>
