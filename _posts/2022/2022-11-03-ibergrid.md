---
title: "HIFIS at the IBERGRID 2022 conference"
title_image: mikael-kristenson-3aVlWP-7bg8-unsplash.jpg
data: 2022-11-03
authors:
  - wetzel
  - foerster
layout: blogpost
additional_css:
  - image-scaling.css
categories:
  - News
tags:
  - conference
  - Jupyterhub
  - in development
  - HIFIS Backbone
  - HIFIS Consulting
excerpt: >
    The Ibergrid Conferences focus on exchange on topics related to integrating infrastructures, enabling services for research, and training scientists in the European Open Science Cloud. This post outlines HIFIS's contributions to this year's conference.
---

<!----------------------->
<!---- Top Paragraph ---->
<!----------------------->
<div class="floating-boxes">

<div class="image-box align-right">
  <a href="https://www.ibergrid.eu/2022-ibergrid-faro/">
    <img
      class="right medium nonuniform"
      alt="a beach"
      src="{% link assets/img/posts/2022-11-04-ibergrid/ibergrid.jpg %}"
      style="width:20%; min-width:250px;"
    >
  </a>
</div>

<div class="text-box" markdown="1"> 

## HIFIS at the IBERGRID 2022

This year's [Ibergrid](https://www.ibergrid.eu/institutional-information/) conference took place in Faro, Portugal and was co-located with the EOSC synergy meeting. 
The University of the Algarve was hosting the four-day event where scientific computing topics of institutions from mostly the Iberian region were discussed. 
The event page [Ibergrid 2022](https://www.ibergrid.eu/2022-ibergrid-faro/) provides the timetable and contributions, of which two were held by members of the HIFIS backbone and software clusters.

Tim Wetzel gave a talk about the [Integrated dataset placement service for scientists](https://indico.lip.pt/event/1249/contributions/4438/), which is envisioned to give scientists the possibility to work with data distributed at several Helmholtz centres directly in a Jupyterhub and corresponding notebooks.
This service would simplify data sharing and collaboration across centres immensely. 
Updates with regard to this and the underlying [data transfer service]({% post_url 2021/10/2021-10-20-use-case-hts %}) will be published on [hifis.net]({% link index.md %}).

Thomas Förster presented the [Helmholtz Federated IT Services: Enabling innovative software and supporting services for research](https://indico.lip.pt/event/1249/contributions/4439/). 
He showed how the [consulting process]({% link consulting.md %}) in Helmholtz works and that users are very pleased with the results and outcomes for them.
A subsequent discussion with members of the auditorium highlighted the need to improve consulting and training offers in the context of FAIR use and common RSE practices, thus confirming our efforts in this area.
</div>
</div>
<!------------------------>

<div class="clear"></div>

## Comments? Suggestions? Questions?

Contact us anytime if you have any feedback you wish to share: <support@hifis.net>

