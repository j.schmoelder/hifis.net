---
title: "Insights from the 12th Incubator workshop"
title_image: jason-goodman-Oalh2MojUuk-unsplash.jpg
date: 2022-05-16
authors:
 - klaffki
 - spicker
layout: blogpost
redirect_from: news/report/event/incubator/2022/05/16/Inkubator-Workshop.html
categories:
  - News
tags:
  - Report
  - Event
  - Incubator
excerpt: >
  The President of the Helmholtz Association, Mr Otmar Wiestler, invited data science experts and the coordinators from these five platforms to meet in Berlin — for the 12th Helmholtz Incubator Workshop.
---

### The Helmholtz Incubator and its plattforms
Since 2016, within the Helmholtz think tank [Incubator Information & Data Science](https://www.helmholtz.de/forschung/challenges/information-data-science/helmholtz-inkubator/) experts from all the Helmholtz Centers come together and promote pioneering Data Science initiatives: from big data analytics, supercomputing, the entire data lifecycle and software development, right up to artificial intelligence and robotics. To further these aspects, five platforms were created, each with a specific purpose: In addition to [HIFIS](https://hifis.net), our partner plattforms are Helmholtz Information & Data Science Academy ([HIDA](https://www.helmholtz-hida.de/en/)), Helmholtz Artificial Intelligence Cooperation Unit ([HelmholtzAI](https://www.helmholtz.ai/)), Helmholtz Imaging ([HIP](https://www.helmholtz-imaging.de/)) and Helmholtz Metadata Collaboration ([HMC](https://helmholtz-metadaten.de/en)).

### The 12th Helmholtz Incubator Workshop
The President of the Helmholtz Association, Mr Otmar Wiestler, invited data science experts and the coordinators from the five platforms to meet in Berlin — for the 12th Helmholtz Incubator Workshop. As it was the first in-person meeting for this group since the Coronavirus pandemic, we were particularly looking forward to it. On the first day, the successes and potential of our activities and platforms in 2021 were evaluated and discussed in breakout groups to gather ideas for the platforms' future concepts. On the second day of the workshop, we started with an exciting panel discussion, featuring the President of the Helmholtz Association and the platform’s Scientific Advisory Board Members reflecting on the achievements and prospects of the platforms. Towards the end, we broadened the view with an input from the Helmholtz Open Science Office from our platforms to the (digital) political landscape, including the German National Research Data Infrastructure (NFDI).

### What HIFIS took home
External guests delivered useful input and constructive ideas for the development of processes around industry-related research and cooperations.
The exchange with stakeholders from outside our plattform has been most valuable: We learned that a lot of aspects of HIFIS are received quite well — 
in fact, we are very pleased to say that we received the best feedback of all five platforms. But we won't stop here, because there is still space for improvement and further developement. 
We cherish the constructive feedback and enlightening perspectives for the further orientation of HIFIS and the other Incubator plattforms and will take them into account.
As a result of this workshop, we sharpened the future development of these plattforms and the overarching Incubator together and look forward to the next months. 
