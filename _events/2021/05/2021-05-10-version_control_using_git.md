---
title: "Version Control using Git"
layout: event
organizers:
  - schlauch
type: workshop
start:
  date:   "2021-05-10"
  time:   "09:00"
end:
  date:   "2021-05-11"
  time:   "13:00"
location:
  campus: "Online"
excerpt:  "This workshop provides an introduction into Git in combination with the collaboration platform GitLab."
registration_period:
  from:   "2021-04-19"
  to:     "2021-04-28"
registration_link:  "https://events.hifis.net/event/102/"
fully_booked_out:  "False"
redirect_from:
  - events/2021/05/10/introduction_to_git_and_gitlab
---
## Goal

The workshop provides a solid introduction into the practical usage of the version control system Git in combination with the collaboration platform GitLab.


## Content

This workshop will cover the the following topics:

 - Introduction to version control
 - Git setup
 - Basic local Git workflow
 - Git branches and handling of conflicts
 - Collaboration with others

Please see the [workshop curriculum](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material#curriculum) for further details.

## Requirements

- No previous knowledge in the covered topics will be required.
 - Participants require a computer equipped with a recent Git command line client, a modern Web browser, and a text editor. We will provide more detailed setup information before the workshop.

We are looking forward to seeing you! 
