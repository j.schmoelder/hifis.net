---
title: HIFIS Transfer Service
title_image: globe.jpeg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
excerpt:
  "An FTS based large data transfer service connecting Helmholtz research."
redirect_to: use-case/2021/10/20/use-case-hts.html
---
